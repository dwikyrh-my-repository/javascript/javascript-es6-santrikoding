// const and let
const first_name = 'dwiky';
let last_name = 'ramadhan h';
console.log(first_name); // dwiky
console.log(last_name); // ramadhan h

// var
var name_one = 'dwiky ramadhan h';
if (true) {
  var name_one = 'putri ayunda';
  console.log(name_one); // putri ayunda
}
console.log(name_one); // putri ayunda

// let
var name_two = 'budi nugraha'; 
if (true) {
  let name_two = 'dania rahmawati';
  console.log(name_two); // dania rahmawati
}
console.log(name_two); // budi nugraha
