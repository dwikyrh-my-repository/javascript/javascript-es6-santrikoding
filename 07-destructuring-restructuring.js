/* destructuring array */
const array = ['Dwiky', 'Ayu', 'Nadya'];
// action destructuring
const [name1, name2, name3] = array;
console.log(name1); // Dwiky
console.log(name2); // Ayu
console.log(name3); // Nadya


/* destructuring object */
const object = {
  name: 'Dwiky Ramadhan H',
  age: 23,
};

// action destructuring object
const { name, age} = object;
console.log(name); // Dwiky Ramadhan H
console.log(age); // 23


/* restructuring */
const fullName = 'Nadya Utari';
const gender = 'female';

// action restructuring
const restructuring = {fullName, gender};
console.log(restructuring); // {name: 'Nadya Utari', gender: 'female'}