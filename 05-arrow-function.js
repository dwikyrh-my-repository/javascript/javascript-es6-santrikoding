// regular function
const hello = function(name) {
  return `hello ${name}`;
}
console.log(hello('dwiky ramadhan h'));

// arrow function with one paramter
const name_one = name => `hello ${name}`;
console.log(name_one('putri ayunda')); // hello putri ayunda

// arrow function with 2 paramter
const name_two = (name, age) => `my name is ${name}, i am ${age} years old`;

console.log(name_two('dwiky ramadhan h', 23)); // my name is dwiky ramadhan h, i am 23 years old

// arrow function object return
const name_three = (name, age) => ({
  status: 'ok',
  message: `my name is ${name}, i am ${age} years old`,
});
console.log(name_three('nadya utari', 22)); // my name is nadya utari, i am 22 years old

// arrow function multiple line
const name_four = (name) => {
  if(!name) {
    return `name is required`;
  }
  return `hello ${name}`;
}
console.log(name_four()); // name is required
console.log(name_four('belinda cyrena')); // belinda cyrena

