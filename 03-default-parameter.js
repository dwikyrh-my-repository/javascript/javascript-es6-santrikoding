function hello(message = 'hello world') {
  console.log(message);
}
// call function without parameter
hello(); // hello world
// call fuuction with parameter
hello('hello indonesia'); // hello indonesia