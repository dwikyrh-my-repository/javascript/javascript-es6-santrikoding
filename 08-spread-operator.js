// spread operator for copy array
const arr1 = [1,2,3,4,5];
const arr2 = [...arr1];

console.log('array 1: ', arr1); // [1,2,3,4,5]
console.log('array 2: ', arr2); // [1,2,3,4,5]

// spread operator for combine array
const arr3 = [1,2,3,];
const arr4 = [4,5,6];
const arr5 = [...arr3, ...arr4];
console.log('combine array: ', arr5); // [1,2,3,4,5,6]
